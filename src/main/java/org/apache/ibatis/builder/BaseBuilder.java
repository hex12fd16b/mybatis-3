/**
 * Copyright 2009-2019 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.builder;

import org.apache.ibatis.mapping.ParameterMode;
import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeAliasRegistry;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.TypeHandlerRegistry;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

// Mybatis 当中各个组件构建器的父类

/**
 * @author Clinton Begin
 */
public abstract class BaseBuilder {
    // 从 XML 文件或者 Java 代码解析出来的配置
    protected final Configuration configuration;
    // 类型别名注册器, 持有所有已经注册的类型别名
    protected final TypeAliasRegistry typeAliasRegistry;
    // 类型处理器注册器, 持有所有已经注册的类型处理器
    protected final TypeHandlerRegistry typeHandlerRegistry;

    // 构造函数, 传入 Mybatis 加载好的配置, 因为类型别名和类型处理器都配置在 Configuration 中
    // 所以通过传入的 Configuration 中的 TypeAliasRegistry 和 TypeHandlerRegistry 也初始化了 Builder 中的字段
    public BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
        this.typeAliasRegistry = this.configuration.getTypeAliasRegistry();
        this.typeHandlerRegistry = this.configuration.getTypeHandlerRegistry();
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    // 解析正则表达式字符串获取字符串对应的正则 Pattern, 如果表达式为 null, 返回解析传入默认值参数的解析后的正则 Pattern
    protected Pattern parseExpression(String regex, String defaultValue) {
        return Pattern.compile(regex == null ? defaultValue : regex);
    }

    // 解析 boolean 字符串获取字符串对应的 Boolean, 如果字符串为 null, 返回传入默认值参数
    protected Boolean booleanValueOf(String value, Boolean defaultValue) {
        return value == null ? defaultValue : Boolean.valueOf(value);
    }

    // 解析 integer 字符串获取字符串对应的 Integer, 如果字符串为 null, 返回传入默认值参数
    protected Integer integerValueOf(String value, Integer defaultValue) {
        return value == null ? defaultValue : Integer.valueOf(value);
    }

    // 解析字符串获取字符串对应的字符串集合, 如果字符串为 null, 返回传入默认值参数解析过后的结果
    protected Set<String> stringSetValueOf(String value, String defaultValue) {
        value = value == null ? defaultValue : value;
        return new HashSet<>(Arrays.asList(value.split(",")));
    }

    // 解析传入别名字符串对应的 Jdbc 类型
    protected JdbcType resolveJdbcType(String alias) {
        if (alias == null) {
            return null;
        }
        // 默认根据 Mybatis 内部枚举 JdbcType 中已经定义的类型别名做解析
        try {
            return JdbcType.valueOf(alias);
        } catch (IllegalArgumentException e) {
            throw new BuilderException("Error resolving JdbcType. Cause: " + e, e);
        }
    }

    // 解析传入别名字符串对应的结果集类型枚举
    protected ResultSetType resolveResultSetType(String alias) {
        if (alias == null) {
            return null;
        }
        // 默认根据 Mybatis 内部枚举 ResultSetType 中已经定义的结果集类型做解析
        try {
            return ResultSetType.valueOf(alias);
        } catch (IllegalArgumentException e) {
            throw new BuilderException("Error resolving ResultSetType. Cause: " + e, e);
        }
    }

    // 解析传入的别名字符串存储过程参数模式
    protected ParameterMode resolveParameterMode(String alias) {
        if (alias == null) {
            return null;
        }
        // 默认根据 Mybatis 内部枚举 ParameterMode 中已经定义的存储过程参数类型做解析
        try {
            return ParameterMode.valueOf(alias);
        } catch (IllegalArgumentException e) {
            throw new BuilderException("Error resolving ParameterMode. Cause: " + e, e);
        }
    }

    // 反射创建别名对应的 Class 信息对应的实例
    protected Object createInstance(String alias) {
        Class<?> clazz = resolveClass(alias);
        if (clazz == null) {
            return null;
        }
        try {
            return resolveClass(alias).newInstance();
        } catch (Exception e) {
            throw new BuilderException("Error creating instance. Cause: " + e, e);
        }
    }

    // 通过别名解析出别名对应的 Class 信息
    protected <T> Class<? extends T> resolveClass(String alias) {
        if (alias == null) {
            return null;
        }
        try {
            return resolveAlias(alias);
        } catch (Exception e) {
            throw new BuilderException("Error resolving class. Cause: " + e, e);
        }
    }

    // 通过 Java 类型和类型处理器别名获取类型处理器
    protected TypeHandler<?> resolveTypeHandler(Class<?> javaType, String typeHandlerAlias) {
        if (typeHandlerAlias == null) {
            return null;
        }
        Class<?> type = resolveClass(typeHandlerAlias);
        // 如果解析出错, 抛出异常
        if (type != null && !TypeHandler.class.isAssignableFrom(type)) {
            throw new BuilderException("Type " + type.getName() + " is not a valid TypeHandler because it does not implement TypeHandler interface");
        }
        @SuppressWarnings("unchecked") // already verified it is a TypeHandler
                Class<? extends TypeHandler<?>> typeHandlerType = (Class<? extends TypeHandler<?>>) type;
        return resolveTypeHandler(javaType, typeHandlerType);
    }

    // 解析类型处理器
    protected TypeHandler<?> resolveTypeHandler(Class<?> javaType, Class<? extends TypeHandler<?>> typeHandlerType) {
        if (typeHandlerType == null) {
            return null;
        }
        // javaType ignored for injected handlers see issue #746 for full detail
        TypeHandler<?> handler = typeHandlerRegistry.getMappingTypeHandler(typeHandlerType);
        // 如果未在已经注册的类型处理器中找到对应的类型处理器, 则通过获取到的类型信息反射创建一个类型处理器
        if (handler == null) {
            // not in registry, create a new one
            handler = typeHandlerRegistry.getInstance(javaType, typeHandlerType);
        }
        return handler;
    }

    // 通过类型别名注册器中持有的信息解析出别名对应的 Class 信息
    protected <T> Class<? extends T> resolveAlias(String alias) {
        // 解析别名到 Class
        return typeAliasRegistry.resolveAlias(alias);
    }
}
