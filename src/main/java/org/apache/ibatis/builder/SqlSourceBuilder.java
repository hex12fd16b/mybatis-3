/**
 * Copyright 2009-2018 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.builder;

import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.parsing.GenericTokenParser;
import org.apache.ibatis.parsing.TokenHandler;
import org.apache.ibatis.reflection.MetaClass;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.JdbcType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// SqlSource 构建器
// MyBatis 中 Mapper 中所有的 SQL 都会被包装成 SqlSource 对象

/**
 * @author Clinton Begin
 */
public class SqlSourceBuilder extends BaseBuilder {

    // 标记变量, 标记参数描述可用的属性
    private static final String parameterProperties = "javaType,jdbcType,mode,numericScale,resultMap,typeHandler,jdbcTypeName";

    // 构造函数, 传入 MyBatis 全局配置类
    public SqlSourceBuilder(Configuration configuration) {
        super(configuration);
    }

    // 需要执行的带参数符的原始 SQL, 参数类型, 附加参数信息构建 SqlSource 对象
    public SqlSource parse(String originalSql, Class<?> parameterType, Map<String, Object> additionalParameters) {
        ParameterMappingTokenHandler handler = new ParameterMappingTokenHandler(configuration, parameterType, additionalParameters);
        GenericTokenParser parser = new GenericTokenParser("#{", "}", handler);
        String sql = parser.parse(originalSql);
        return new StaticSqlSource(configuration, sql, handler.getParameterMappings());
    }

    // 参数映射标记处理器
    private static class ParameterMappingTokenHandler extends BaseBuilder implements TokenHandler {
        // 参数映射集合
        private List<ParameterMapping> parameterMappings = new ArrayList<>();
        // 参数类型 Class
        private Class<?> parameterType;
        // 参数元对象信息
        private MetaObject metaParameters;

        public ParameterMappingTokenHandler(Configuration configuration, Class<?> parameterType, Map<String, Object> additionalParameters) {
            // 传入 MyBatis 全局配置初始化 BaseBuilder
            super(configuration);
            this.parameterType = parameterType;
            // 通过传入的附加参数构建元对象信息
            this.metaParameters = configuration.newMetaObject(additionalParameters);
        }

        public List<ParameterMapping> getParameterMappings() {
            return parameterMappings;
        }

        @Override
        public String handleToken(String content) {
            parameterMappings.add(buildParameterMapping(content));
            return "?";
        }

        // 通过内容构建参数映射信息
        private ParameterMapping buildParameterMapping(String content) {
            // 将参数内容转换为各个域, 包括属性名, JdbcType 等
            Map<String, String> propertiesMap = parseParameterMapping(content);
            String property = propertiesMap.get("property");
            Class<?> propertyType;
            // 获取属性对应的类型 Class
            if (metaParameters.hasGetter(property)) { // issue #448 get type from additional params
                // 判断类型元信息中是否有解析出的属性, 如果有就赋值给 propertyType
                propertyType = metaParameters.getGetterType(property);
            } else if (typeHandlerRegistry.hasTypeHandler(parameterType)) {
                // 判断类型处理器注册信息中是否有构造时传入的参数类型对应的类型解析器, 如果有就将构造时传入的参数类型赋值给 propertyType
                propertyType = parameterType;
            } else if (JdbcType.CURSOR.name().equals(propertiesMap.get("jdbcType"))) {
                // 如果传入参数内容中解析出来参数的类型是游标, 直接设置属性类型为 java.sql.ResultSet.class
                propertyType = java.sql.ResultSet.class;
            } else if (property == null || Map.class.isAssignableFrom(parameterType)) {
                // 如果解析出的 property 为 NULL, 或者参数类型是 Map, 直接将属性类型设置为 Object.class
                propertyType = Object.class;
            } else {
                // 最后, 通过类型元信息去找 property 对应的字段类型, 如果没有找到, 将属性类型设置为 Object.class
                MetaClass metaClass = MetaClass.forClass(parameterType, configuration.getReflectorFactory());
                if (metaClass.hasGetter(property)) {
                    propertyType = metaClass.getGetterType(property);
                } else {
                    propertyType = Object.class;
                }
            }
            // 准备好构建一个 ParameterMapping 的所有参数之后, 构造好 ParameterMapping 然后返回
            ParameterMapping.Builder builder = new ParameterMapping.Builder(configuration, property, propertyType);
            Class<?> javaType = propertyType;
            String typeHandlerAlias = null;
            for (Map.Entry<String, String> entry : propertiesMap.entrySet()) {
                String name = entry.getKey();
                String value = entry.getValue();
                if ("javaType".equals(name)) {
                    javaType = resolveClass(value);
                    builder.javaType(javaType);
                } else if ("jdbcType".equals(name)) {
                    builder.jdbcType(resolveJdbcType(value));
                } else if ("mode".equals(name)) {
                    builder.mode(resolveParameterMode(value));
                } else if ("numericScale".equals(name)) {
                    builder.numericScale(Integer.valueOf(value));
                } else if ("resultMap".equals(name)) {
                    builder.resultMapId(value);
                } else if ("typeHandler".equals(name)) {
                    typeHandlerAlias = value;
                } else if ("jdbcTypeName".equals(name)) {
                    builder.jdbcTypeName(value);
                } else if ("property".equals(name)) {
                    // Do Nothing
                } else if ("expression".equals(name)) {
                    throw new BuilderException("Expression based parameters are not supported yet");
                } else {
                    throw new BuilderException("An invalid property '" + name + "' was found in mapping #{" + content + "}.  Valid properties are " + parameterProperties);
                }
            }
            if (typeHandlerAlias != null) {
                builder.typeHandler(resolveTypeHandler(javaType, typeHandlerAlias));
            }
            return builder.build();
        }

        // 将参数内容转换为参数表达式, 参数表达式中已经把一个表达式中的各个表达式域解析完成
        // 参数表达式是 HashMap 的子类
        private Map<String, String> parseParameterMapping(String content) {
            try {
                return new ParameterExpression(content);
            } catch (BuilderException ex) {
                throw ex;
            } catch (Exception ex) {
                throw new BuilderException("Parsing error was found in mapping #{" + content + "}.  Check syntax #{property|(expression), var1=value1, var2=value2, ...} ", ex);
            }
        }
    }

}
