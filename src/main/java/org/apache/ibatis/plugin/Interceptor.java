/**
 * Copyright 2009-2015 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.plugin;

import java.util.Properties;

// MyBatis 插件接口

/**
 * @author Clinton Begin
 */
public interface Interceptor {

    // 拦截执行的目标
    Object intercept(Invocation invocation) throws Throwable;

    // 利用这个接口生成代理对象, MyBatis 中使用 Plugin.warp 方法完成对象的动态代理
    Object plugin(Object target);

    // 设置属性
    void setProperties(Properties properties);

}
