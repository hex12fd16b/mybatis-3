/**
 * Copyright 2009-2015 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.type;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// Mybatis 类型处理器接口定义
// Mybatis 默认提供了一些类型处理器注册在 TypeHandlerRegistry
// 自定义的类型处理器也需要实现这个接口

/**
 * @author Clinton Begin
 */
public interface TypeHandler<T> {

    // 设置参数到预编译的 JDBC Sql Statement, 类型参数的传入解析在这个实现
    void setParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException;

    // 将 JDBC 执行结果集中指定列名的结果解析为特定类型的结果, 特定的类型由泛型参数 T 标识
    T getResult(ResultSet rs, String columnName) throws SQLException;

    // 将 JDBC 执行结果集中指定列序号的结果解析为特定类型的结果, 特定的类型由泛型参数 T 标识
    T getResult(ResultSet rs, int columnIndex) throws SQLException;

    // CallableStatement 对象是所有的关系数据库提供了一种以标准形式调用已储存过程的方法
    // 将 JDBC 存储过程执行结果集中指定列序号的结果解析为特定类型的结果, 特定的类型由泛型参数 T 标识
    T getResult(CallableStatement cs, int columnIndex) throws SQLException;

}
