/**
 * Copyright 2009-2015 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.logging;

// MyBatis 框架中对日志框架的顶层抽象

/**
 * @author Clinton Begin
 */
public interface Log {

    // 是否开启调试日志输出
    boolean isDebugEnabled();

    // 是否开启诊断日志输出
    boolean isTraceEnabled();

    // 输出错误日志
    void error(String s, Throwable e);

    // 输出错误日志
    void error(String s);

    // 输出调试日志
    void debug(String s);

    // 输出诊断日志
    void trace(String s);

    // 输出警告日志
    void warn(String s);

}
