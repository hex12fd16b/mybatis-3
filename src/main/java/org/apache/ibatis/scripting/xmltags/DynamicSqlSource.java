/**
 * Copyright 2009-2017 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.scripting.xmltags;

import org.apache.ibatis.builder.SqlSourceBuilder;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.session.Configuration;

import java.util.Map;

// 动态 Sql 源

/**
 * @author Clinton Begin
 */
public class DynamicSqlSource implements SqlSource {
    // MyBatis 全局配置
    private final Configuration configuration;
    // 动态 SQL 内容 XML 根节点
    private final SqlNode rootSqlNode;

    // 构造函数
    public DynamicSqlSource(Configuration configuration, SqlNode rootSqlNode) {
        this.configuration = configuration;
        this.rootSqlNode = rootSqlNode;
    }

    // 获取经过 MyBatis 动态上下文处理过后的 SQL 对象
    @Override
    public BoundSql getBoundSql(Object parameterObject) {
        // 构建动态上下文
        DynamicContext context = new DynamicContext(configuration, parameterObject);
        // 传入动态构建上下文, 由 SQL 描述根节点应用动态上下文
        rootSqlNode.apply(context);
        // SqlSource 转换器
        SqlSourceBuilder sqlSourceParser = new SqlSourceBuilder(configuration);
        Class<?> parameterType = parameterObject == null ? Object.class : parameterObject.getClass();
        // 转换 SQL 文本, 将其中的 #{xxx}, 替换为 ?, 将内容中的表达式解析为对应的参数名, 最后将参数名和对象内属性和参数的映射关系包装成一个 StaticSqlSource
        SqlSource sqlSource = sqlSourceParser.parse(context.getSql(), parameterType, context.getBindings());
        // 传入真实的参数, 获取 BoundSql 对象
        BoundSql boundSql = sqlSource.getBoundSql(parameterObject);
        for (Map.Entry<String, Object> entry : context.getBindings().entrySet()) {
            boundSql.setAdditionalParameter(entry.getKey(), entry.getValue());
        }
        return boundSql;
    }

}
