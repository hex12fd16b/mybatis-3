/**
 * Copyright 2009-2018 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.executor;

// 定义 Mybatis 中的错误上下文, 用 ThreadLocal 做了线程隔离
// 定义了描述错误的几个元素

/**
 * @author Clinton Begin
 */
public class ErrorContext {
    // 多个错误行的隔离字符, 如果运行时环境变量中未获取到, 默认使用 "\n" ( *nix 风格的换行符)
    private static final String LINE_SEPARATOR = System.getProperty("line.separator", "\n");
    // 错误上下文多线程隔离 ThreadLocal
    private static final ThreadLocal<ErrorContext> LOCAL = new ThreadLocal<>();
    // 寄存功能中存储错误上下文的引用标记
    private ErrorContext stored;
    // 资源描述字符串
    private String resource;
    // 执行的行为
    private String activity;
    // 关联的对象
    private String object;
    // 消息
    private String message;
    // SQL 字符串
    private String sql;
    // 错误上下文中的异常
    private Throwable cause;

    // 隐藏构造函数
    private ErrorContext() {
    }

    // 获取当前线程上下文中的单例, 从 ThreadLocal 中获取, 如果不存在, 则初始化
    public static ErrorContext instance() {
        ErrorContext context = LOCAL.get();
        if (context == null) {
            context = new ErrorContext();
            LOCAL.set(context);
        }
        return context;
    }

    // 寄存运行时错误上下文, 初始化一个新的错误上下文放在 ThreadLocal 中, 旧的错误上下文设置在实例变量级别的引用当中
    public ErrorContext store() {
        ErrorContext newContext = new ErrorContext();
        newContext.stored = this;
        LOCAL.set(newContext);
        return LOCAL.get();
    }

    // 如果错误上下文寄存器中寄存有错误上下文, 从寄存器中寄存的错误上下文中恢复错误上下文; 否则, 返回当前 ThreadLocal 中的错误上下文
    public ErrorContext recall() {
        if (stored != null) {
            LOCAL.set(stored);
            stored = null;
        }
        return LOCAL.get();
    }

    // Builder 风格的代码, 设置 resource
    public ErrorContext resource(String resource) {
        this.resource = resource;
        return this;
    }

    // Builder 风格的代码, 设置 activity
    public ErrorContext activity(String activity) {
        this.activity = activity;
        return this;
    }

    // Builder 风格的代码, 设置 object
    public ErrorContext object(String object) {
        this.object = object;
        return this;
    }

    // Builder 风格的代码, 设置 message
    public ErrorContext message(String message) {
        this.message = message;
        return this;
    }

    // Builder 风格的代码, 设置 sql
    public ErrorContext sql(String sql) {
        this.sql = sql;
        return this;
    }

    // Builder 风格的代码, 设置 cause
    public ErrorContext cause(Throwable cause) {
        this.cause = cause;
        return this;
    }

    // 重置错误上下文, 将实例变量指向的地址都设置为 null, 移除 ThreadLocal 中设置的变量 (GC 友好型的编码都是这样)
    public ErrorContext reset() {
        resource = null;
        activity = null;
        object = null;
        message = null;
        sql = null;
        cause = null;
        LOCAL.remove();
        return this;
    }

    // 自定义 toString
    @Override
    public String toString() {
        StringBuilder description = new StringBuilder();

        // message
        if (this.message != null) {
            description.append(LINE_SEPARATOR);
            description.append("### ");
            description.append(this.message);
        }

        // resource
        if (resource != null) {
            description.append(LINE_SEPARATOR);
            description.append("### The error may exist in ");
            description.append(resource);
        }

        // object
        if (object != null) {
            description.append(LINE_SEPARATOR);
            description.append("### The error may involve ");
            description.append(object);
        }

        // activity
        if (activity != null) {
            description.append(LINE_SEPARATOR);
            description.append("### The error occurred while ");
            description.append(activity);
        }

        // activity
        if (sql != null) {
            description.append(LINE_SEPARATOR);
            description.append("### SQL: ");
            description.append(sql.replace('\n', ' ').replace('\r', ' ').replace('\t', ' ').trim());
        }

        // cause
        if (cause != null) {
            description.append(LINE_SEPARATOR);
            description.append("### Cause: ");
            description.append(cause.toString());
        }

        return description.toString();
    }

}
