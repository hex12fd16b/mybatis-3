/**
 * Copyright 2009-2015 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.executor;

import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.transaction.Transaction;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Clinton Begin
 */
// MyBatis Sql 执行器
public interface Executor {

    // 标记变量, 标记没有结果的结果处理器
    ResultHandler NO_RESULT_HANDLER = null;

    // 更新操作
    int update(MappedStatement ms, Object parameter) throws SQLException;

    // 查询操作
    <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey cacheKey, BoundSql boundSql) throws SQLException;

    // 查询操作
    <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler) throws SQLException;

    // 查询操作, 返回 MyBatis Cursor
    <E> Cursor<E> queryCursor(MappedStatement ms, Object parameter, RowBounds rowBounds) throws SQLException;

    // 将批处理中的 Statement 刷新到数据库中执行
    List<BatchResult> flushStatements() throws SQLException;

    // 提交
    void commit(boolean required) throws SQLException;

    // 回滚
    void rollback(boolean required) throws SQLException;

    // 创建缓存 Key
    CacheKey createCacheKey(MappedStatement ms, Object parameterObject, RowBounds rowBounds, BoundSql boundSql);

    // 是否缓存
    boolean isCached(MappedStatement ms, CacheKey key);

    // 清除本地缓存
    void clearLocalCache();

    // 懒加载
    void deferLoad(MappedStatement ms, MetaObject resultObject, String property, CacheKey key, Class<?> targetType);

    // 获取事务
    Transaction getTransaction();

    // 关闭
    void close(boolean forceRollback);

    // 是否关闭
    boolean isClosed();

    // 设置执行器包装
    void setExecutorWrapper(Executor executor);

}
