/**
 * Copyright 2009-2016 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.parsing;

import java.util.Properties;

// 属性转换器

/**
 * @author Clinton Begin
 * @author Kazuki Shimizu
 */
public class PropertyParser {

    // 前缀
    private static final String KEY_PREFIX = "org.apache.ibatis.parsing.PropertyParser.";
    /**
     * The special property key that indicate whether enable a default value on placeholder.
     * <p>
     * The default value is {@code false} (indicate disable a default value on placeholder)
     * If you specify the {@code true}, you can specify key and default value on placeholder (e.g. {@code ${db.username:postgres}}).
     * </p>
     *
     * @since 3.4.2
     */
    // 标志常量, 标记属性转换过程中是否允许使用默认值占位符
    public static final String KEY_ENABLE_DEFAULT_VALUE = KEY_PREFIX + "enable-default-value";

    /**
     * The special property key that specify a separator for key and default value on placeholder.
     * <p>
     * The default separator is {@code ":"}.
     * </p>
     *
     * @since 3.4.2
     */
    // 标志常量, 标记默认的属性值分隔符, 默认的属性分隔符为 ":"
    public static final String KEY_DEFAULT_VALUE_SEPARATOR = KEY_PREFIX + "default-value-separator";
    // 标志常量, 标志默认禁用默认值
    private static final String ENABLE_DEFAULT_VALUE = "false";
    // 标志常量, 默认的属性分隔符为 ":"
    private static final String DEFAULT_VALUE_SEPARATOR = ":";

    // 私有化构造函数
    private PropertyParser() {
        // Prevent Instantiation
    }

    // 转换属性的方法
    public static String parse(String string, Properties variables) {
        // VariableTokenHandler 的构造函数已经被私有化了, 但是 VariableTokenHandler 是 PropertyParser 的私有静态内部类
        // 所以 PropertyParser 能够访问 VariableTokenHandler 的构造函数
        VariableTokenHandler handler = new VariableTokenHandler(variables);
        GenericTokenParser parser = new GenericTokenParser("${", "}", handler);
        return parser.parse(string);
    }

    // 变量标识处理器
    // Mybatis 的变量中, ${xxx} 的标记由 VariableTokenHandler 处理, 被处理过后, 直接替换成将 ${xxx} 替换成属性的值
    private static class VariableTokenHandler implements TokenHandler {
        // 变量属性
        private final Properties variables;
        // 是否允许默认值
        private final boolean enableDefaultValue;
        // 默认的属性值分隔符
        private final String defaultValueSeparator;

        // 私有化构造函数
        private VariableTokenHandler(Properties variables) {
            this.variables = variables;
            this.enableDefaultValue = Boolean.parseBoolean(getPropertyValue(KEY_ENABLE_DEFAULT_VALUE, ENABLE_DEFAULT_VALUE));
            this.defaultValueSeparator = getPropertyValue(KEY_DEFAULT_VALUE_SEPARATOR, DEFAULT_VALUE_SEPARATOR);
        }

        // 根据传入的 key, 从变量 properties 中获取对应的属性值
        private String getPropertyValue(String key, String defaultValue) {
            return (variables == null) ? defaultValue : variables.getProperty(key, defaultValue);
        }

        @Override
        // 处理标记
        public String handleToken(String content) {
            if (variables != null) {
                String key = content;
                if (enableDefaultValue) {
                    // 如果设置了允许默认值, 根据默认值语法解析 aaa:bbb 默认值分隔符的字符索引序号
                    final int separatorIndex = content.indexOf(defaultValueSeparator);
                    String defaultValue = null;
                    if (separatorIndex >= 0) {
                        // 从 content 中取出 property 的 key
                        key = content.substring(0, separatorIndex);
                        // 从 content 中取出 property 的默认值
                        defaultValue = content.substring(separatorIndex + defaultValueSeparator.length());
                    }
                    if (defaultValue != null) {
                        // 如果默认值不为空, 根据 java.util.Properties.getProperty(java.lang.String, java.lang.String) 获取属性的值
                        return variables.getProperty(key, defaultValue);
                    }
                }
                if (variables.containsKey(key)) {
                    return variables.getProperty(key);
                }
            }
            // 如果传入变量属性为空, 跳过属性内容解析, 将内容包装为 ${xxxx} 的格式返回
            return "${" + content + "}";
        }
    }

}
