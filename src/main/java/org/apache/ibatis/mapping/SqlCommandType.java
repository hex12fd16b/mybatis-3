/**
 * Copyright 2009-2015 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.mapping;

// Mybatis Sql 命令类型
// SqlCommandType 描述 Mapper 中解析过后的 MappedStatement 需要执行的动作类型

/**
 * @author Clinton Begin
 */
public enum SqlCommandType {
    // 未知类型
    UNKNOWN,
    // 插入
    INSERT,
    // 更新
    UPDATE,
    // 更新
    DELETE,
    // 查询
    SELECT,
    // 刷新
    FLUSH;
}
