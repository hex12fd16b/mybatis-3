/**
 * Copyright 2009-2018 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.mapping;

import java.sql.ResultSet;

/**
 * @author Clinton Begin
 */
// 查询结果集类型
public enum ResultSetType {
    /**
     * behavior with same as unset (driver dependent).
     *
     * @since 3.5.0
     */
    // 默认, 相当于不设置
    DEFAULT(-1),
    // 结果集的游标职能向下滚动
    FORWARD_ONLY(ResultSet.TYPE_FORWARD_ONLY),
    // 结果集的游标可以上下移动, 在遍历的时候, 当查询的表中的结果可能会影响到结果时, 当前结果集不变
    SCROLL_INSENSITIVE(ResultSet.TYPE_SCROLL_INSENSITIVE),
    // 结果集的游标可以上下移动, 在遍历的时候, 当查询的表中的结果可能会影响到结果时, 当前结果集会变
    SCROLL_SENSITIVE(ResultSet.TYPE_SCROLL_SENSITIVE);

    private final int value;

    ResultSetType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
