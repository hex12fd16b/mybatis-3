/**
 * Copyright 2009-2019 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.annotations;

import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.mapping.StatementType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// 指定 Mapper 方法的设置属性
// 类似于以文件形式配置 Mapper 在 statement 项(<select>, <update> ...) 中设置

/**
 * @author Clinton Begin
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Options {
    /**
     * The options for the {@link Options#flushCache()}.
     * The default is {@link FlushCachePolicy#DEFAULT}
     */
    // 清空缓存的策略
    enum FlushCachePolicy {
        /**
         * <code>false</code> for select statement; <code>true</code> for insert/update/delete statement.
         */
        // 默认
        DEFAULT,
        /**
         * Flushes cache regardless of the statement type.
         */
        // 强制清除缓存
        TRUE,
        /**
         * Does not flush cache regardless of the statement type.
         */
        // 强制不刷新清除
        FALSE
    }

    // 是否使用缓存
    boolean useCache() default true;

    // 缓存清除策略
    FlushCachePolicy flushCache() default FlushCachePolicy.DEFAULT;

    ResultSetType resultSetType() default ResultSetType.DEFAULT;

    StatementType statementType() default StatementType.PREPARED;

    int fetchSize() default -1;

    int timeout() default -1;

    boolean useGeneratedKeys() default false;

    String keyProperty() default "";

    String keyColumn() default "";

    String resultSets() default "";
}
