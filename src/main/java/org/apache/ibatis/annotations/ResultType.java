/**
 * Copyright 2009-2019 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// 这个注解可以指定 Mapper 中标注有 @Select 注解的方法的结果的类型, 将结果的映射方式通过 ResultHandler 指定
// 需要注意的是, 标注这个注解的 Mapper 方法的返回值类型必须是 void
// 例如:
// DefaultResultHandler handler = new DefaultResultHandler()
// mapper.collectRangeBlogs(handler, new RowBounds(1, 1))
// @Select({
//          "SELECT *",
//          "FROM blog",
//          "ORDER BY id"
//  })
//  @ResultType(Blog.class)
//  void collectRangeBlogs(ResultHandler<Object> blog, RowBounds rowBounds);

/**
 * This annotation can be used when a @Select method is using a
 * ResultHandler.  Those methods must have void return type, so
 * this annotation can be used to tell MyBatis what kind of object
 * it should build for each row.
 *
 * @author Jeff Butler
 * @since 3.2.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ResultType {
    Class<?> value();
}
