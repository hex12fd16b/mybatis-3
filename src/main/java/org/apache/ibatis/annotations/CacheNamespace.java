/**
 * Copyright 2009-2019 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.annotations;

import org.apache.ibatis.cache.decorators.LruCache;
import org.apache.ibatis.cache.impl.PerpetualCache;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// 用于隔离 MyBatis 二级缓存, 等同于在 MyBatis Mapper 文件中指定 <cache/> 或者 设置 <cache-ref namespace="xxx" />
// 在二级缓存的使用中, 一个 namespace 下的所有的操作语句, 都影响着同一个 Cache, 也就是说二级缓存是被多个 SqlSession 所共享着的, 是一个全局变量。
// 当开启缓存后, 数据的查询执行的流程是 二级缓存 -> 一级缓存 -> 数据库
// 有一篇介绍 Mybatis 缓存的文章写得很好: https://www.jianshu.com/p/c553169c5921

/**
 * @author Clinton Begin
 * @author Kazuki Shimizu
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CacheNamespace {
    // 缓存使用的类型, 默认是使用 PerpetualCache
    Class<? extends org.apache.ibatis.cache.Cache> implementation() default PerpetualCache.class;

    // 缓存的淘汰策略, 默认使用 LRU (Latest Recent Used) 算法淘汰
    Class<? extends org.apache.ibatis.cache.Cache> eviction() default LruCache.class;

    // 缓存的刷新间隔, 单位是毫秒
    long flushInterval() default 0;

    // 缓存的大小
    int size() default 1024;

    // 是否可读写
    boolean readWrite() default true;

    // 若缓存中找不到对应的key，是否会一直 blocking，直到有对应的数据进入缓存
    boolean blocking() default false;

    /**
     * Property values for a implementation object.
     *
     * @since 3.4.2
     */
    // 属性
    Property[] properties() default {};

}
