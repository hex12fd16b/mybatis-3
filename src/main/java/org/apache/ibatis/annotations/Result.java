/**
 * Copyright 2009-2016 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.annotations;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.UnknownTypeHandler;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// 在注解 @Results 中使用, 指定 SQL 结果集到 Java POJO 的映射关系

/**
 * @author Clinton Begin
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({})
public @interface Result {
    // 是否是 ID
    boolean id() default false;

    // 结果集中的列名
    String column() default "";

    // Java POJO 中的属性名
    String property() default "";

    // Java POJO 属性的类型 class
    Class<?> javaType() default void.class;

    // 结果集中列名的 JDBC 类型
    JdbcType jdbcType() default JdbcType.UNDEFINED;

    // 类型处理器
    Class<? extends TypeHandler> typeHandler() default UnknownTypeHandler.class;

    // 一对一级联
    One one() default @One;

    // 一对多级联
    Many many() default @Many;
}
