/**
 * Copyright 2009-2018 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.annotations;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.UnknownTypeHandler;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//
// Mybatis 通过注解自动将 SQL 查询结果映射到 POJO 的一个辅助注解, 配合 @ConstructorArgs 使用
// 示例用法:
// @ConstructorArgs({
//   @Arg(column= "foo_id", javaType=Integer.class),
//   @Arg(column= "foo_attr", javaType=String.class)
// })
// Select("select * from FooBar where foo_id=#{fooId}"
// public Foo getFooBy(@Param("fooId") Integer fooId);
//

/**
 * @author Clinton Begin
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({})
public @interface Arg {
    // 是否是主键
    boolean id() default false;

    // sql 结果中的列名
    String column() default "";

    // pojo 对应的 java 类型
    Class<?> javaType() default void.class;

    // JDBC 类型
    JdbcType jdbcType() default JdbcType.UNDEFINED;

    // 类型处理器
    Class<? extends TypeHandler> typeHandler() default UnknownTypeHandler.class;

    // 级联查询时通过指定的 SQL 构建 POJO 类型的 Field
    String select() default "";

    // 结果映射集
    String resultMap() default "";

    // 指定 POJO 字段的名字
    String name() default "";

    /**
     * @since 3.5.0
     */
    // 自定列名的前缀
    String columnPrefix() default "";
}
