/**
 * Copyright 2009-2016 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// @Case 注解需要配合 @TypeDiscriminator 使用
// mybatis 的 mapper 可以使用 discriminator 来根据 SQL 结果的某一列来动态地映射 ResultSet
// 中的 column 到 POJO 的 field
// 例如:
// @TypeDiscriminator(
//      column = "draft",
//      javaType = String.class,
//      cases = {@Case(value = "1", type = DraftPost.class)}
//  )

/**
 * @author Clinton Begin
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({})
public @interface Case {
    // 条件值, 相当于当外层 @TypeDiscriminator 中的 column 字段的值为多少的时候触发条件映射
    String value();

    // 映射的 POJO 的类型
    Class<?> type();

    // 指定映射结果
    Result[] results() default {};

    // 指定映射的构造参数
    Arg[] constructArgs() default {};
}
