/**
 * Copyright 2009-2019 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// 可以使用这个注解, 从 mybatis 配置文件中的 <properties/> 域中指定获取一个预先设置的属性注入到执行名称的属性
// 例如:
// @CacheNamespace(implementation = SupportClasses.CustomCache.class, properties = {
//         @Property(name = "name", value = "${cache.name:default}")
// })

/**
 * The annotation that inject a property value.
 *
 * @author Kazuki Shimizu
 * @see CacheNamespace
 * @since 3.4.2
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({})
public @interface Property {

    /**
     * A target property name.
     */
    // 目标属性名
    String name();

    /**
     * A property value or placeholder.
     */
    // 源属性的占位符
    String value();
}