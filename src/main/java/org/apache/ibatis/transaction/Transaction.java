/**
 * Copyright 2009-2019 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.transaction;

import java.sql.Connection;
import java.sql.SQLException;

// MyBatis 事务接口
// 负责管理 Jdbc 连接的整个生命周期

/**
 * Wraps a database connection.
 * Handles the connection lifecycle that comprises: its creation, preparation, commit/rollback and close.
 *
 * @author Clinton Begin
 */
public interface Transaction {

    /**
     * Retrieve inner database connection.
     *
     * @return DataBase connection
     * @throws SQLException
     */
    // 获取一个 Jdbc 连接
    Connection getConnection() throws SQLException;

    /**
     * Commit inner database connection.
     *
     * @throws SQLException
     */
    // 提交 Jdbc 连接上的事务
    void commit() throws SQLException;

    /**
     * Rollback inner database connection.
     *
     * @throws SQLException
     */
    // 回滚 Jdbc 连接上的事务
    void rollback() throws SQLException;

    /**
     * Close inner database connection.
     *
     * @throws SQLException
     */
    // 关闭 Jdbc 连接
    void close() throws SQLException;

    /**
     * Get transaction timeout if set.
     *
     * @throws SQLException
     */
    // 获取事务的超时时间
    Integer getTimeout() throws SQLException;

}
